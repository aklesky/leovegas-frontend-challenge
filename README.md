# README #

LeoVegas Frontend Challenge

### Quick summary ###
The Single Page Application (SPA) of the Hackernews site using Reactjs.

Frontend part is done by React and Apollo-React.
Backend part is fully working with apollo-express-server and graphql ( NodeJS ).

### Requirements ###

The SPA will need to display a set of items received from an API (https://hn.algolia.com/api).
The initial load of the page will show the latest news (http://hn.algolia.com/api/v1/search?tags=front_page).

Requirements:

* A link anywhere in the section should lead to the article’s URL.
* A search option is needed, any new search needs to update the page.
* A reload button should be available for the user to get fresh results.
* Sorting options (disabled by default): by title alphabetical order, by date.

### Sorting ###

* Sort by Title (done) by default
* Sort By Date (done) ascending/descending


### UI Features ###

* Floating action button is fetching / reloading existing content
* SearchBar will filter by Article Title
* Infinity Scroll List will display records by 20 articles per page (Paging)
* Clicking on the item will open a new browser tab
* Icon Menu contains sorting elements


### NOT INCLUDED ###
* manifest.json
* favicon, apple icons, chrome icons
* service workers
* UI E2E tests
* Offline Support

### Demo ###

[https://aklesky-leovegas-challenge.herokuapp.com/](https://aklesky-leovegas-challenge.herokuapp.com/)

### CORE ###
* [react](https://facebook.github.io/react/)
* [graphql](http://graphql.org/)
* [graphql-tag](https://github.com/apollographql/graphql-tag)
* [react-apollo](https://github.com/apollographql/react-apollo)
* [apollo-express-server](https://github.com/apollographql/apollo-server)


# Project Structure
* src
* src/app
* src/app/queries (**apollo client queries**)
* src/app/components
* src/app/components/news (**the main component to view and query hacker news articles**)
* src/app/components/list (**infinity scroll list to view hacker news articles**)
* src/app/config
* src/app/layouts
* src/app/providers
* src/app/containers
* src/app/translations
* src/app.js (**Entrypoint**)
* src/index.html (**Main Index html file**)
* data (**Api calls to hackernews api**)
* data/schemas (**Graphql Schemas**)
* config (**Apollo Express Server**)
* config/env


# Local installation / dev

* install npm packages
```sh
npm i
```

```
* run shell command
```sh
npm run dev
```

### Node Version ###
* node v8.7.0
* npm 5.5.1

### Continuous Integration and Delivery ###
* Circle CI ( eslint test)
* Circle CI Continuous Delivery to Heroku

### UI ###
* [Material UI](http://http://www.material-ui.com/)
