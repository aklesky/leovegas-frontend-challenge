import React from 'react';
import { render } from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import AppContainer from './app/containers/AppContainer';
import { ApolloProvider, ThemeProvider } from './app/providers/';

import './static/css/app.scss';

injectTapEventPlugin();

render(
  <ApolloProvider>
    <ThemeProvider>
      <AppContainer />
    </ThemeProvider>
  </ApolloProvider>,
  document.getElementById('app'),
);
