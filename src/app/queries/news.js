import gql from 'graphql-tag';
import { ascending, descending } from '../utils/sorting';

const HackerNewsQuery = gql`
  query hackerNewsQuery($page: Int, $sortBy: String, $sort: String, $search: String) {
    getNews(page: $page, sort: $sort, sortBy: $sortBy, search: $search) {
      currentPage
      pageCount
      articles {
        author
        title
        objectID
        points
        num_comments
        created_at
        url
      }
    }
  }
`;

const QueryOptions = {
  options() {
    return {
      variables: {
        page: 0,
        sortBy: 'title',
        sort: 'asc',
      },
      fetchPolicy: 'cache-first',
    };
  },
  props({
    data: {
      loading, getNews, fetchMore, refetch, variables,
    },
  }) {
    return {
      loading,
      getNews,
      refetch,
      variables,
      loadMoreEntries(page) {
        return fetchMore({
          variables: {
            page,
          },
          updateQuery: (previousResult, params) => {
            if (!params.fetchMoreResult) {
              return previousResult;
            }

            if (params.fetchMoreResult.getNews.currentPage === previousResult.getNews.currentPage) {
              return previousResult;
            }

            const articles = [...previousResult.getNews.articles, ...params.fetchMoreResult.getNews.articles];

            if (params.variables.sort === 'desc') {
              articles.sort((a, b) => descending(a, b, params.variables.sortBy));
            } else {
              articles.sort((a, b) => ascending(a, b, params.variables.sortBy));
            }

            return Object.assign({}, previousResult, {
              loading: false,
              getNews: {
                ...params.fetchMoreResult.getNews,
                articles,
              },
            });
          },
        });
      },
    };
  },
};

module.exports = {
  HackerNewsQuery,
  QueryOptions,
};
