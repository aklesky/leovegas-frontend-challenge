import React from 'react';
import PropTypes from 'prop-types';
import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import SearchBar from 'material-ui-search-bar';

import { toolbar, toolGroup } from './styles';

class ToolbarFilter extends React.PureComponent {
  static propTypes = {
    onSearch: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      search: null,
    };
  }

  render() {
    return (
      <Toolbar
        style={{
          width: '100%',
          padding: 0,
        }}
      >
        <ToolbarGroup
          style={toolbar}
        >
          <SearchBar
            onChange={(value) => {
              this.setState({ search: value });
              this.props.onSearch(value);
            }}
            onRequestSearch={() => this.props.onSearch(this.state.search)}
            style={toolGroup}
          />
        </ToolbarGroup>
      </Toolbar>
    );
  }
}

export default ToolbarFilter;
