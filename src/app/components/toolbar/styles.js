module.exports = {
  toolbar: {
    width: '100%',
    padding: 0,
  },
  toolGroup: {
    margin: '0 auto',
    maxWidth: '100%',
    width: '100%',
  },
};
