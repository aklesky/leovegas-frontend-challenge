import React from 'react';
import PropTypes from 'prop-types';
import { List } from 'material-ui/List';

class InfiniteList extends React.PureComponent {
  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
    loadMore: PropTypes.func.isRequired,
    ref: PropTypes.func,
    threshold: PropTypes.number,
    hasNext: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    ref: null,
    threshold: 250,
  };

  constructor(props) {
    super(props);

    this.scrollListener = this.scrollListener.bind(this);
  }

  componentDidMount() {
    this.attachScrollListener();
  }

  componentWillReceiveProps(newProps) {
    if (newProps.hasNext === false) {
      this.detachScrollListener();
    }

    if (newProps.hasNext && this.props.hasNext === false) {
      this.attachScrollListener();
    }
  }

  componentWillUnmount() {
    this.detachScrollListener();
  }

  detachScrollListener() {
    window.removeEventListener('scroll', this.scrollListener, true);
  }

  attachScrollListener() {
    window.addEventListener('scroll', this.scrollListener, true);
  }

  scrollListener() {
    const el = this.scrollComponent;
    const scrollTop = window.pageYOffset !== undefined ? window.pageYOffset : (document.documentElement || document.body).scrollTop;

    const topPosition = this.calculateTopPosition(el);
    const offset = topPosition + (el.offsetHeight - scrollTop - window.innerHeight);
    if (offset < Number(this.props.threshold)) {
      if (this.props.hasNext) {
        this.props.loadMore();
      } else {
        this.detachScrollListener();
      }
    }
  }

  calculateTopPosition(el) {
    if (!el) {
      return 0;
    }
    return el.offsetTop + this.calculateTopPosition(el.offsetParent);
  }

  render() {
    const { children, ref } = this.props;
    return (
      <section
        ref={(node) => {
          this.scrollComponent = node;
          if (ref) {
            ref(node);
          }
        }}
      >
        <List>{children}</List>
      </section>
    );
  }
}

export default InfiniteList;
