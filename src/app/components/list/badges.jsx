import React from 'react';
import PropTypes from 'prop-types';
import TrendingUp from 'material-ui/svg-icons/action/trending-up';
import Comment from 'material-ui/svg-icons/communication/comment';
import Badge from 'material-ui/Badge';
import IconButton from 'material-ui/IconButton';

import { comments, points } from './styles';

export const PointsBadge = props => (
  <Badge
    className='PointsBadge__root'
    primary
    badgeContent={props.points}
    badgeStyle={points}
  >
    <IconButton tooltip={props.tooltip}>
      <TrendingUp />
    </IconButton>
  </Badge>
);
export const CommentsBadge = props => (
  <Badge
    className='CommentsBadge__root'
    primary
    badgeContent={props.points || 0}
    badgeStyle={comments}
  >
    <IconButton tooltip={props.tooltip}>
      <Comment />
    </IconButton>
  </Badge>
);


CommentsBadge.defaultProps = {
  points: 0,
  tooltip: '',
};
CommentsBadge.propTypes = {
  points: PropTypes.number,
  tooltip: PropTypes.string,
};

PointsBadge.defaultProps = {
  points: 0,
  tooltip: '',
};
PointsBadge.propTypes = {
  points: PropTypes.number,
  tooltip: PropTypes.string,
};
