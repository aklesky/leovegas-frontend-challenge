module.exports = {
  points: {
    top: 5,
    right: 20,
    fontSize: 9,
  },
  comments: {
    top: 5,
    right: 5,
    fontSize: 9,
  },
};
