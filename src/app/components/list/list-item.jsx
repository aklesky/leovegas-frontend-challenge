import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { ListItem } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import { injectIntl, intlShape } from 'react-intl';

import {
  CommentsBadge,
  PointsBadge,
} from './badges';

class Item extends React.PureComponent {
  static defaultProps = {
    url: null,
    points: 0,
    num_comments: 0,
  }
  static propTypes = {
    title: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired,
    points: PropTypes.number,
    num_comments: PropTypes.number,
    created_at: PropTypes.string.isRequired,
    url: PropTypes.string,
    intl: intlShape.isRequired,
  };

  onClick = () => {
    if (!this.props.url) {
      return;
    }
    window.open(this.props.url, '_blank');
  };

  render() {
    const publishDate = moment(new Date(this.props.created_at));
    const publishDateFormatted = moment(this.props.created_at).format('DD/MM/YYYY');
    const { formatMessage } = this.props.intl;
    return (
      <section>
        <ListItem
          onClick={this.onClick}
          secondaryText={
            <section>
              <p style={{ padding: 0, margin: 0 }}>Author: {this.props.author}</p>
              <p style={{ padding: 0, margin: 0 }}>{publishDateFormatted} - {publishDate.fromNow()}</p>
            </section>
          }
          primaryText={this.props.title}
          leftIcon={<PointsBadge tooltip={formatMessage({ id: 'points', defaultMessage: 'Trending' })} points={this.props.points} />}
          rightIcon={<CommentsBadge tooltip={formatMessage({ id: 'comments', defaultMessage: 'Comments' })} points={this.props.num_comments} />}
          secondaryTextLines={2}
        />
        <Divider inset />
      </section>
    );
  }
}


export default injectIntl(Item);
