import React from 'react';
import PropTypes from 'prop-types';
import { List, ListItem } from '../list';

const NewsComponent = props => (
  <section>
    {props.getNews.articles &&
      props.getNews.articles.length > 0 && (
        <List
          sort={props.sort}
          threshold={650}
          loadMore={props.loadMoreEntries}
          hasNext={props.getNews.currentPage < props.getNews.pageCount - 1}
        >
          {props.getNews.articles.map(item => <ListItem key={item.objectID} {...item} />)}
        </List>
      )}
  </section>
);

NewsComponent.defaultProps = {
  getNews: {},
};
NewsComponent.propTypes = {
  loadMoreEntries: PropTypes.func.isRequired,
  sort: PropTypes.string.isRequired,
  getNews: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.shape({
      articles: PropTypes.arrayOf(PropTypes.object),
      currentPage: PropTypes.number,
      pageCount: PropTypes.number,
    }),
  ]),
};

export default NewsComponent;
