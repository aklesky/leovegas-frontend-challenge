import React from 'react';
import PropTypes from 'prop-types';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Toggle from 'material-ui/Toggle';
import Menu from 'material-ui/Menu';
import Divider from 'material-ui/Divider';
import { white } from 'material-ui/styles/colors';
import { injectIntl, intlShape } from 'react-intl';

const SortingMenu = (props) => {
  const {
    intl,
    sort,
    sortByDirection,
    sortProperty,
  } = props;
  const { formatMessage } = intl;
  return (
    <IconMenu
      useLayerForClickAway
      iconButtonElement={
        <IconButton>
          <MoreVertIcon color={white} />
        </IconButton>
      }
      targetOrigin={{ horizontal: 'right', vertical: 'top' }}
      anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
    >
      <Menu disableAutoFocus width={250}>
        <MenuItem
          leftIcon={<Toggle toggled={props.sort === 'asc'} />}
          primaryText={sort === 'asc' ?
            formatMessage({ id: 'asc', defaultMessage: 'Asc' }) :
            formatMessage({ id: 'desc', defaultMessage: 'Desc' })}
          onClick={sortByDirection}
        />
        <Divider />
        <MenuItem
          onClick={() => props.sortBy('title')}
          primaryText={formatMessage({ id: 'byTitle', defaultMessage: 'Title' })}
          checked={sortProperty === 'title'}
        />
        <MenuItem
          onClick={() => props.sortBy('created_at')}
          primaryText={formatMessage({ id: 'byDate', defaultMessage: 'Date' })}
          checked={sortProperty === 'created_at'}
        />
      </Menu>
    </IconMenu>
  );
};

SortingMenu.propTypes = {
  sort: PropTypes.string.isRequired,
  sortProperty: PropTypes.string.isRequired,
  sortBy: PropTypes.func.isRequired,
  sortByDirection: PropTypes.func.isRequired,
  intl: intlShape.isRequired,
};

export default injectIntl(SortingMenu);
