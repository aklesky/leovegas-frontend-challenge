import React from 'react';
import PropTypes from 'prop-types';
import AppBar from 'material-ui/AppBar';
import { injectIntl, intlShape } from 'react-intl';
import SortingMenu from './menu';

const Header = ({
  intl, label, sort, sortBy, sortByDirection, sortProperty,
}) => {
  const { formatMessage } = intl;
  return (
    <AppBar
      title={formatMessage({ id: label, defaultMessage: 'Aleksei Semiglasov App' })}
      showMenuIconButton={false}
      iconElementRight={<SortingMenu sortProperty={sortProperty} sort={sort} sortBy={sortBy} sortByDirection={sortByDirection} />}
    />
  );
};

Header.defaultProps = {
  label: 'appTitle',
};

Header.propTypes = {
  label: PropTypes.string,
  sort: PropTypes.string.isRequired,
  sortBy: PropTypes.func.isRequired,
  sortByDirection: PropTypes.func.isRequired,
  sortProperty: PropTypes.string.isRequired,
  intl: intlShape.isRequired,
};

export default injectIntl(Header);
