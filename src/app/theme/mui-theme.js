import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {
  deepOrange500,
  blueGrey800,
  deepOrange700,
  orange700,
  blueGrey600,
} from 'material-ui/styles/colors';
import { mui } from './metrics';

const muiTheme = getMuiTheme({
  ...mui,
  palette: {
    primary1Color: deepOrange700,
    accent1Color: orange700,
  },
  textField: {
    errorColor: deepOrange500,
  },
  raisedButton: {
    primaryColor: blueGrey800,
  },
  icon: {
    color: blueGrey600,
  },
  svgIcon: {
    color: blueGrey600,
  },
});

module.exports = muiTheme;
