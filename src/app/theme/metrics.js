const mui = {
  spacing: {
    iconSize: 24,
    desktopGutter: 16,
    desktopGutterMore: 26,
    desktopGutterLess: 10,
    desktopGutterMini: 2,
    desktopKeylineIncrement: 64,
    desktopDropDownMenuItemHeight: 24,
    desktopDropDownMenuFontSize: 12,
  },
  menuItem: {
    dataHeight: 24,
    height: 24,
  },
  borderRadius: 2,
};

const snackbarStyles = {
  style: {
    bottom: 95,
  },
};

module.exports = {
  mui,
  snackbarStyles,
};
