import MainLayout from './MainLayout';
import NewsLayout from './NewsLayout';

module.exports = {
  MainLayout,
  NewsLayout,
};
