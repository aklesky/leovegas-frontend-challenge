import React from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import { paperStyles } from './styles';

const MainLayout = props => (
  <Paper style={paperStyles.paper}>
    {props.children}
  </Paper>
);

MainLayout.defaultProps = {
  children: null,
};

MainLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.element,
  ]),
};

export default MainLayout;
