import React from 'react';
import PropTypes from 'prop-types';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Cached from 'material-ui/svg-icons/action/cached';

import { Header, News, Toolbar } from '../components';

import MainLayout from './MainLayout';

const NewsLayout = props => (
  <MainLayout>
    <Header
      sortByDirection={props.sortByDirection}
      sort={props.sort}
      sortBy={props.sortBy}
      sortProperty={props.sortProperty}
    />
    <FloatingActionButton className='app-fab' onTouchTap={props.onRefetch} secondary>
      <Cached />
    </FloatingActionButton>
    <Toolbar onSearch={props.onSearch} />
    <section className='margin-vertical-5'>
      <News sort={props.sort} loadMoreEntries={props.loadMoreEntries} getNews={props.getNews} />
    </section>
  </MainLayout>
);

NewsLayout.defaultProps = {
  getNews: {},
  sort: 'asc',
  sortProperty: 'title',
};

NewsLayout.propTypes = {
  loadMoreEntries: PropTypes.func.isRequired,
  sortByDirection: PropTypes.func.isRequired,
  getNews: PropTypes.shape({
    articles: PropTypes.arrayOf(PropTypes.object),
    currentPage: PropTypes.number,
    pageCount: PropTypes.number,
  }),
  onRefetch: PropTypes.func.isRequired,
  onSearch: PropTypes.func.isRequired,
  sort: PropTypes.string,
  sortProperty: PropTypes.string,
  sortBy: PropTypes.func.isRequired,
};

export default NewsLayout;
