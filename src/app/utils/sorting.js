const ascending = (a, b, sortBy) => {
  if (a[sortBy] < b[sortBy]) {
    return -1;
  } else if (a[sortBy] > b[sortBy]) {
    return 1;
  }
  return 0;
};
const descending = (a, b, sortBy) => {
  if (a[sortBy] > b[sortBy]) {
    return -1;
  } else if (a[sortBy] < b[sortBy]) {
    return 1;
  }
  return 0;
};

module.exports = {
  ascending,
  descending,
};
