import React from 'react';
import NewsContainer from './NewsContainer';
import { ApolloProvider, ThemeProvider, IntlProvider } from '../providers';

export default () => (
  <ApolloProvider>
    <ThemeProvider>
      <IntlProvider locale='en'>
        <NewsContainer />
      </IntlProvider>
    </ThemeProvider>
  </ApolloProvider>
);
