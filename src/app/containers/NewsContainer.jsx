import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';

import { NewsLayout } from '../layouts';
import { HackerNewsQuery, QueryOptions } from '../queries/news';
import { smoothScrollToTop } from '../utils/scrollable';

class NewsContainer extends React.PureComponent {
  static defaultProps = {
    getNews: {
      currentPage: 0,
      articles: [],
    },
  };
  static propTypes = {
    loadMoreEntries: PropTypes.func.isRequired,
    getNews: PropTypes.shape({
      articles: PropTypes.arrayOf(PropTypes.object),
      currentPage: PropTypes.number,
      pageCount: PropTypes.number,
    }),
    refetch: PropTypes.func.isRequired,
    variables: PropTypes.shape({
      page: PropTypes.number.isRequired,
      sort: PropTypes.string.isRequired,
      sortBy: PropTypes.string.isRequired,
      search: PropTypes.string,
    }).isRequired,
  };

  constructor(props) {
    super(props);
    this.onRefetch = this.onRefetch.bind(this);
    this.onSortByDirection = this.onSortByDirection.bind(this);
    this.onSortBy = this.onSortBy.bind(this);
    this.onSearch = this.onSearch.bind(this);
  }


  onRefetch() {
    this.props.refetch({
      sort: this.props.variables.sort,
      sortBy: this.props.variables.sortBy,
      search: null,
    });
    smoothScrollToTop(1000);
  }

  onSortByDirection() {
    this.props.refetch({
      sort: this.props.variables.sort === 'asc' ? 'desc' : 'asc',
      sortBy: this.props.variables.sortBy,
      search: this.props.variables.search,
    });
  }

  onSortBy(sortBy, direction = null) {
    this.props.refetch({
      sort: direction || this.props.variables.sort,
      sortBy,
      search: this.props.variables.search,
    });
  }

  onSearch(value) {
    const search = value ? value.trim() : null;
    this.props.refetch({
      sort: this.props.variables.sort,
      sortBy: this.props.variables.sortBy,
      search,
    });
  }

  render() {
    return (
      <NewsLayout
        onRefetch={this.onRefetch}
        loadMoreEntries={() => this.props.loadMoreEntries(this.props.getNews.currentPage + 1)}
        getNews={this.props.getNews}
        sortByDirection={this.onSortByDirection}
        sortBy={this.onSortBy}
        sortProperty={this.props.variables.sortBy}
        sort={this.props.variables.sort}
        onSearch={this.onSearch}
      />
    );
  }
}

export default graphql(HackerNewsQuery, QueryOptions)(NewsContainer);
