import React from 'react';
import PropTypes from 'prop-types';
import { ApolloProvider } from 'react-apollo';

import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

export const apolloClient = new ApolloClient({
  link: new HttpLink(),
  cache: new InMemoryCache(),
});

const Apollo = ({ children }) => <ApolloProvider client={apolloClient}>{children}</ApolloProvider>;

Apollo.defaultProps = {
  children: null,
};

Apollo.propTypes = {
  children: PropTypes.element,
};

export default Apollo;
