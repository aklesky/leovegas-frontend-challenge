import axios from 'axios';

const config = require('../../config/env/config');

describe('Hacker hews api suite', () => {
  let client = null;

  const configuration = { ...config.api };

  beforeEach(() => {
    client = axios.create(configuration);
  });

  afterEach(() => {
    client = null;
  });

  it('axios request should return status 200', async () => {
    const response = await client.get('search?tags=front_page');
    const { status } = response;
    expect(status).toBeTruthy();
    expect(status).toBe(200);
  });

  it('axios request should return data', async () => {
    const response = await client.get('search?tags=front_page');
    const { data } = response;
    expect(data).toBeTruthy();
    expect(data).toHaveProperty('hits');
    expect(Array.isArray(data.hits)).toBeTruthy();
    expect(data.hits.length).toBeGreaterThanOrEqual(1);
  });
});
