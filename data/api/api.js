const axios = require('axios');
const env = require('../../config/env/config');

let instance = null;

class Client {
  constructor(configuration) {
    this.client = axios.create(configuration);
    this.getHackerNews = this.getHackerNews.bind(this);
  }

  /**
   *
   *
   * @returns
   * @memberof Client
   */
  getHackerNews(page = 0) {
    return this.client
      .get(`search?tags=front_page&page=${page}`)
      .then(response => response.data);
  }
  /**
   *
   *
   * @returns
   * @memberof Client
   */
  searchHackerNews(search, page) {
    return this.client
      .get(`search?query=${search}&tags=front_page&page=${page}`)
      .then(response => response.data);
  }
}

const config = Object.assign({}, env.api);

module.exports = {
  getClient: () => {
    if (!instance) {
      instance = new Client(config);
    }
    return instance;
  },
};
