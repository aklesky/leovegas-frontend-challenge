
import { getClient } from './api';

describe('Api client suite:', () => {
  it('getClient should not be undefined', () => {
    expect(getClient).not.toBeUndefined();
  });

  it('getClient should be singleton', () => {
    const singleton = getClient();
    const instance = getClient();
    expect(singleton).toEqual(instance);
  });

  it('getClient should contain the getHackerNews method', () => {
    const client = getClient();
    expect(client).toHaveProperty('getHackerNews');
  });

  it('getHackerNews should return an array of articles', async () => {
    const client = getClient();
    const response = await client.getHackerNews();
    expect(response).toBeTruthy();
    expect(response).toHaveProperty('hits');
    expect(response.hits.length).toBeGreaterThanOrEqual(1);
  });
});
