const { makeExecutableSchema } = require('graphql-tools');

const hackerNews = require('./schema/hacker-news');
const { hackerNewsResolver } = require('./resolvers');

const executableSchema = makeExecutableSchema({
  typeDefs: [hackerNews],
  resolvers: hackerNewsResolver,
});
module.exports = executableSchema;
