const sorting = require('./sorting');

module.exports = {
  hackerNewsQuery: {
    getNews: async (parent, {
      page, sort, sortBy, search,
    }, { client }) => {
      const response = search ? await client.searchHackerNews(search, page) :
        await client.getHackerNews(page);

      const articles = response.hits || [];

      if (sort === 'desc') {
        articles.sort((a, b) => sorting.descending(a, b, sortBy));
      } else {
        articles.sort((a, b) => sorting.ascending(a, b, sortBy));
      }

      return {
        articles,
        currentPage: response.page,
        pageCount: response.nbPages || 0,
      };
    },
  },
};
