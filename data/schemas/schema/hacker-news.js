const Article = require('./article');

const News = `
  type News {
    pageCount: Int
    currentPage: Int
    articles: [Article]
  }

  type hackerNewsQuery {
    getNews(page: Int, sort: String, sortBy: String, search: String): News
  }

  schema {
    query: hackerNewsQuery
  }

`;

module.exports = () => [News, Article];
