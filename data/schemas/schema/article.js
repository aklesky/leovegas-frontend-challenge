const Article = `
  type Article {
    objectID: ID!
    title: String!
    url: String
    author: String
    points: Int
    story_text: String
    comment_text: String
    created_at_i: Int
    created_at: String
    num_comments: Int
  }
`;

module.exports = Article;
