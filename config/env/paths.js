const path = require('path');
const fs = require('fs');

const appDirectory = fs.realpathSync(process.cwd());

const resolve = relativePath => path.resolve(appDirectory, relativePath);

module.exports = {
  appBuild: resolve('dist'),
  appHtml: resolve('src/index.html'),
  appIndex: resolve('src/app.js'),
  appSrc: resolve('src'),
  nodeNodules: resolve('node_modules'),
  dataPath: resolve('data'),
};
