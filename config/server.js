const apolloServer = require('./apollo');
const devApp = require('./dev.server');
const config = require('./env/config');
const { getClient } = require('../data/api/api');

const client = getClient();

const env = process.env.NODE_ENV || 'production';
const port = process.env.PORT || 3000;

if (env === 'development') {
  apolloServer(config.host, config.devGraphQLPort, { client }, true);
  devApp();
  return;
}

apolloServer(config.host, port, { client });
